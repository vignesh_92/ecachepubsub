package com.javasampleapproach.redis.pubsub.producer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.listener.ChannelTopic;

import com.javasampleapproach.redis.pubsub.model.Customer;

public class RedisCustomerInfoPublisher implements CustomerInfoPublisher {

	List<Customer> customers = new ArrayList<>(Arrays.asList(new Customer(1, "MRF", "85000.52"),new Customer(2, "PAGE", "20000.54"),
			new Customer(3, "MRF", "85000.90"), new Customer(5, "PAGE", "20000.64"),
			new Customer(4, "3M", "13500.56"), new Customer(6, "3M", "13500.74")));

	private final AtomicInteger counter = new AtomicInteger(0);

	@Autowired
	private RedisTemplate<String, Object> redisTemplate;

	@Autowired
	private ChannelTopic topic;

	@Autowired
	private StringRedisTemplate stringRedisTemplate;

	public RedisCustomerInfoPublisher() {
	}

	public RedisCustomerInfoPublisher(RedisTemplate<String, Object> redisTemplate, ChannelTopic topic) {
		this.redisTemplate = redisTemplate;
		this.topic = topic;
	}

	@Override
	public void publish() {
		Customer customer = customers.get(counter.getAndIncrement());
		ValueOperations<String, String> values = stringRedisTemplate.opsForValue();
		values.set(customer.getFirstName(), customer.getLastName());
		System.out.println(
				"Publishing... with id=" + customer.getFirstName() + ", " + Thread.currentThread().getName());

		redisTemplate.convertAndSend(customer.getFirstName(), customer.toString());
	}

}
